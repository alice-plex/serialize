# Changelog

## 2.2.0

* Change `aired` to date in yml

## 2.1.2

* Update `aliceplex-schema` to 3.1.2

## 2.1.1

* Update `aliceplex-schema` to 3.1.1

## 2.1.0

* Update `aliceplex-schema` to 3.1.0

## 2.0.1

* Add pytest on setup

## 2.0.0

* Refactor to aliceplex
