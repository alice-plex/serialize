# aliceplex-serialize

aliceplex-serialize is serialization library for Plex.

## Install

```bash
pip install aliceplex-serialize
```
