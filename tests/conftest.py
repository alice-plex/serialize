from datetime import date
from typing import List

from aliceplex.schema import Actor, Album, Artist, Episode, Movie, Person, Show
from pytest import fixture

from aliceplex.serialize.base import Serializer


@fixture
def actors() -> List[Actor]:
    actors = []
    for i in range(1, 4):
        actors.append(Actor(name=f"name {i}",
                            role=f"role {i}",
                            photo=f"photo {i}"))
    return actors


@fixture
def actors_no_photo() -> List[Actor]:
    actors = []
    for i in range(1, 4):
        actors.append(Actor(name=f"name {i}",
                            role=f"role {i}"))
    return actors


@fixture
def person() -> Person:
    return Person(name="name", photo="photo")


@fixture
def person_no_photo() -> Person:
    return Person(name="name")


@fixture
def actor() -> Actor:
    return Actor(name="name", role="role", photo="photo")


@fixture
def actor_no_photo() -> Actor:
    return Actor(name="name", role="role")


@fixture
def show(actor_no_photo: Actor, dummy_date: date) -> Show:
    return Show(
        title="title",
        sort_title="sort_title",
        original_title=["original_title"],
        content_rating="content_rating",
        tagline=["tagline"],
        studio=["studio"],
        aired=dummy_date,
        summary="summary",
        rating=1,
        genres=["genres"],
        collections=["collections"],
        actors=[actor_no_photo],
        season_summary={1: "Season 1 Summary"}
    )


@fixture
def show_no_summary(actor_no_photo: Actor, dummy_date: date) -> Show:
    return Show(
        title="title",
        sort_title="sort_title",
        original_title=["original_title"],
        content_rating="content_rating",
        tagline=["tagline"],
        studio=["studio"],
        aired=dummy_date,
        summary="summary",
        rating=1,
        genres=["genres"],
        collections=["collections"],
        actors=[actor_no_photo],
    )


@fixture
def episode(dummy_date: date, person_no_photo: Person) -> Episode:
    return Episode(
        title=["title"],
        content_rating="content_rating",
        aired=dummy_date,
        summary="summary",
        rating=1,
        writers=[person_no_photo],
        directors=[person_no_photo]
    )


@fixture
def movie(actor_no_photo: Actor,
          dummy_date: date,
          person_no_photo: Person) -> Movie:
    return Movie(
        title="title",
        sort_title="sort_title",
        original_title=["original_title"],
        content_rating="content_rating",
        tagline=["tagline"],
        studio=["studio"],
        aired=dummy_date,
        summary="summary",
        rating=1,
        genres=["genres"],
        collections=["collections"],
        actors=[actor_no_photo],
        writers=[person_no_photo],
        directors=[person_no_photo]
    )


@fixture
def artist() -> Artist:
    return Artist(
        summary="summary",
        collections=["collections"],
        genres=["genres"],
        similar=["similar"]
    )


@fixture
def album(dummy_date: date) -> Album:
    return Album(
        aired=dummy_date,
        collections=["collections"],
        genres=["genres"],
        summary="summary",
    )


@fixture
def dummy_date() -> date:
    return date(2018, 1, 1)


@fixture
def dummy_date_str() -> str:
    return "2018-01-01"


@fixture
def serializer() -> Serializer:
    return Serializer()


def pytest_generate_tests(metafunc):
    if "model" in metafunc.fixturenames:
        metafunc.parametrize("model", [
            show_impl(actor_no_photo_impl(), dummy_date_impl()),
            Show(),
            episode_impl(dummy_date_impl(), person_no_photo_impl()),
            Episode(),
            movie_impl(
                actor_no_photo_impl(),
                dummy_date_impl(),
                person_no_photo_impl()
            ),
            Movie(),
            album_impl(dummy_date_impl()),
            Album(),
            artist_impl(),
            Artist(),
            actor_impl(),
            Actor()
        ])


# TODO: Remove work around.
# calling-fixtures-directly deprecation reports wrong module and fixture name
# https://github.com/pytest-dev/pytest/issues/4619
def actors_impl() -> List[Actor]:
    actors = []
    for i in range(1, 4):
        actors.append(Actor(name=f"name {i}",
                            role=f"role {i}",
                            photo=f"photo {i}"))
    return actors


def actors_no_photo_impl() -> List[Actor]:
    actors = []
    for i in range(1, 4):
        actors.append(Actor(name=f"name {i}",
                            role=f"role {i}"))
    return actors


def person_impl() -> Person:
    return Person(name="name", photo="photo")


def person_no_photo_impl() -> Person:
    return Person(name="name")


def actor_impl() -> Actor:
    return Actor(name="name", role="role", photo="photo")


def actor_no_photo_impl() -> Actor:
    return Actor(name="name", role="role")


def show_impl(actor_no_photo: Actor, dummy_date: date) -> Show:
    return Show(
        title="title",
        sort_title="sort_title",
        original_title=["original_title"],
        content_rating="content_rating",
        tagline=["tagline"],
        studio=["studio"],
        aired=dummy_date,
        summary="summary",
        rating=1,
        genres=["genres"],
        collections=["collections"],
        actors=[actor_no_photo],
        season_summary={1: "Season 1 Summary"}
    )


def show_no_summary_impl(actor_no_photo: Actor, dummy_date: date) -> Show:
    return Show(
        title="title",
        sort_title="sort_title",
        original_title=["original_title"],
        content_rating="content_rating",
        tagline=["tagline"],
        studio=["studio"],
        aired=dummy_date,
        summary="summary",
        rating=1,
        genres=["genres"],
        collections=["collections"],
        actors=[actor_no_photo],
    )


def episode_impl(dummy_date: date, person_no_photo: Person) -> Episode:
    return Episode(
        title=["title"],
        content_rating="content_rating",
        aired=dummy_date,
        summary="summary",
        rating=1,
        writers=[person_no_photo],
        directors=[person_no_photo]
    )


def movie_impl(actor_no_photo: Actor,
               dummy_date: date,
               person_no_photo: Person) -> Movie:
    return Movie(
        title="title",
        sort_title="sort_title",
        original_title=["original_title"],
        content_rating="content_rating",
        tagline=["tagline"],
        studio=["studio"],
        aired=dummy_date,
        summary="summary",
        rating=1,
        genres=["genres"],
        collections=["collections"],
        actors=[actor_no_photo],
        writers=[person_no_photo],
        directors=[person_no_photo]
    )


def artist_impl() -> Artist:
    return Artist(
        summary="summary",
        collections=["collections"],
        genres=["genres"],
        similar=["similar"]
    )


def album_impl(dummy_date: date) -> Album:
    return Album(
        aired=dummy_date,
        collections=["collections"],
        genres=["genres"],
        summary="summary",
    )


def dummy_date_impl() -> date:
    return date(2018, 1, 1)


def dummy_date_str_impl() -> str:
    return "2018-01-01"


def serializer_impl() -> Serializer:
    return Serializer()
