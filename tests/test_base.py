from pathlib import Path

from aliceplex.schema import Artist
from pytest import raises
from pytest_mock import MockFixture

from aliceplex.serialize.base import Serializer


def test_serialize(serializer: Serializer,
                   mocker: MockFixture,
                   artist: Artist):
    method = mocker.patch.object(serializer, "_serialize")
    serializer.serialize(Path(""), artist)
    assert method.called

    with raises(NotImplementedError):
        serializer.serialize(Path(""), {})
