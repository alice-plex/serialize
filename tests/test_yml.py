from os import remove
from pathlib import Path

from aliceplex.serialize import YmlSerializer
from aliceplex.serialize.base import Model


def test_model(model: Model):
    serializer = YmlSerializer()
    path = Path("temp.yml")
    try:
        serializer.serialize(path, model)
        new_model = serializer.deserialize(path)
        assert model == new_model
    finally:
        if path.exists():
            remove(path)
