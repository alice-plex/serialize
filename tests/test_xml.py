from pathlib import Path

from aliceplex.schema import Episode, Movie, Show

from aliceplex.serialize import XmlSerializer


def test_show(show_no_summary: Show):
    serializer = XmlSerializer()

    path = Path("tests/test_xml/show.xml")
    model = serializer.deserialize(path)
    assert model == show_no_summary

    path = Path("tests/test_xml/show2.xml")
    model = serializer.deserialize(path)
    assert model == Show()


def test_movie(movie: Movie):
    serializer = XmlSerializer()

    path = Path("tests/test_xml/movie.xml")
    model = serializer.deserialize(path)
    assert model == movie

    path = Path("tests/test_xml/movie2.xml")
    model = serializer.deserialize(path)
    assert model == Movie()


def test_episode(episode: Episode):
    serializer = XmlSerializer()

    path = Path("tests/test_xml/episode.xml")
    model = serializer.deserialize(path)
    assert model == episode

    path = Path("tests/test_xml/episode2.xml")
    model = serializer.deserialize(path)
    assert model == Episode()
